require("dotenv").config()

module.exports = {
  siteMetadata: {
    title: "Portfolio",
    description: "Personal portfolio and resume page of Niko Soininen",
    author: "Niko Soininen",
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-source-strapi",
      options: {
        apiURL: process.env.API_URL || "http://localhost:1337",
        contentTypes: [
          // List of the Content Types you want to be able to request from Gatsby.
          //"user",
          "color",
          "page",
          "project",
          "technology",
          "association",
          "experience",
          "experience-type",
        ],
        singleTypes: [
          
        ],
        queryLimit: 1000,
      },
    },
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "gatsby-starter-default",
        short_name: "starter",
        start_url: "/",
        background_color: "#663399",
        theme_color: "#663399",
        display: "minimal-ui",
        icon: "src/images/fav-big.png",
      },
    },
    "gatsby-plugin-offline",
    {
      resolve: "gatsby-plugin-zopfli",
      options: {
        extensions: ["css", "html", "js", "svg", "json"],
      },
    },
    `gatsby-plugin-fontawesome-css`,
  ],
}
