import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Layout from "../components/main/layout"
import Feature from "../components/blocks/feature"
import Test from "../components/content/testcontent"
import Welcome from "../components/content/welcome"
import Image from "../components/blocks/image"
import FeaturedProjects from "../components/content/featuredProjects"

const IndexPage = () => {
  const data = useStaticQuery(graphql`
    query {
      strapiPage(title: {eq: "Welcome"}) {
        cover {
          childImageSharp {
            fluid(maxWidth: 1080) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  `)
  return(
    <Layout subTitle="Welcome" extended={true} 
      feature={
        <Welcome/>
      }
      featureImg={
        data.strapiPage.cover
      }>
        <FeaturedProjects></FeaturedProjects>
      
    </Layout>
  )
}

export default IndexPage
