import React from "react"

import Layout from "../components/main/layout"
import SEO from "../components/main/seo"

const title = "404: Not found"

const NotFoundPage = () => (
  <Layout>
    <SEO title={title} />
    <h1>NOT FOUND</h1>
    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </Layout>
)

export default NotFoundPage
