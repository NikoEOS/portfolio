import React from "react"

import Layout from "../components/main/layout"

const IndexPage = () => <Layout subTitle={"Portfolio"}>Portfolio</Layout>

export default IndexPage
