import React, { useState } from 'react';

import Layout from "../components/main/layout"
import Experience from "../components/blocks/experienceBlock"


export const query = graphql`
query AllStrapiExperience {
  allStrapiExperience {
    nodes {
      association {
        name
      }
      experience_type {
        name
        color
      }
      id
      endDate
      startDate
      position
    }
  }
}
`

const Experiences = ({ data }) => {
  const [experiences, setExperiences] = useState(data.allStrapiExperience.nodes);

  return (
    <Layout subTitle="Experiences" extended={false}>  
      {
        experiences.map(element => {
          return(
            <div key={element.id} className="card margin-bottom-base">
              <Experience data={element}></Experience>
            </div>
          )
        })
      }

    </Layout>
  );
};

export default Experiences;
