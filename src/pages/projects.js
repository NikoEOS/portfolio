import React, { useState } from 'react';
import { graphql, Link } from "gatsby";
import Layout from "../components/main/layout";
import Project from "../components/blocks/projectBlock"


export const query = graphql`
query AllStrapiProject {
  allStrapiProject {
    nodes {
      id
      title
      slug
      technologies {
        id
        color
        name
      }
      associations {
        name
      }
      cover {
        childImageSharp {
          fluid(maxWidth: 150) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
}
`


const Projects = ({ data }) => {
  const [projects, setProjects] = useState(data.allStrapiProject.nodes);

  return (
    <Layout subTitle="Projects" extended={false}>
      {
        projects.map(project => {
          return(
            <div key={project.id}>
              <Link
                className="card margin-bottom-base scale"
                to={"projects/" + project.slug}
              >
                <Project
                  title={project.title}
                  cover={project.cover}
                  associations={project.associations}
                  technologies={project.technologies}
                ></Project>
              </Link>
            </div>
          )
        })
      }
    </Layout>
  );
};

export default Projects;