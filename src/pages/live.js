import React from "react"

import Layout from "../components/main/layout"

const IndexPage = () => <Layout subTitle={"Live apps"}>Live</Layout>

export default IndexPage
