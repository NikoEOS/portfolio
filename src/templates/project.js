import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/main/layout";
import ReactMarkdown from "react-markdown"
import Img from "gatsby-image"

const ProjectTemplate = ({ data }) => {
  const project = data.strapiProject

  return (
    <Layout extended={false}
      subTitle={project.title + " project"}
      featureImg = {project.cover}
    >
      <h1>{project.title}</h1>
      <ReactMarkdown
        source={project.description}
      />
    </Layout>
  );
};

export const pageQuery = graphql`
  query ProjectDetailsById($id: String) {
    strapiProject(id: {eq: $id}) {
      id
      title
      description
      cover {
        childImageSharp {
          fluid(maxWidth: 960) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`

export default ProjectTemplate;