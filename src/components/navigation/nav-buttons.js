
import React from "react"
import { Link, StaticQuery, graphql } from "gatsby"
import PropTypes from "prop-types"
import { useNavData } from "./useNavData"

const NavButtons = () => {
  const allStrapiPage = useNavData()
  return allStrapiPage.edges.map((e, i) => {
    return (
      <Link
        key={e.node.strapiId}
        to={`/${e.node.slug}`}
        className="btn shadow color-dark"
        style={{ backgroundColor: e.node.color.hex }}
      >
        {e.node.title}
      </Link>
    )
  })
}

export default NavButtons

