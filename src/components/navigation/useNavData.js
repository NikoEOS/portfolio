
import { useStaticQuery, graphql } from "gatsby"
export const useNavData = () => {
  const { allStrapiPage } = useStaticQuery(
    graphql`
      query {
        allStrapiPage(filter: {isNav: {eq: true}}) {
          edges {
            node {
              strapiId
              title
              slug
              color {
                hex
              }
            }
          }
        }
      }
    `
  )

  return allStrapiPage
}
