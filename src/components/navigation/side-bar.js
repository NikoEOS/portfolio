import React from "react"
import { Link, StaticQuery, graphql } from "gatsby"
import PropTypes from "prop-types"
import { useNavData } from "./useNavData"

const SideBar = () => {
  const allStrapiPage = useNavData()
  return allStrapiPage.edges.map((e, i) => {
    return (
      <div>
        <h1>{e.node.slug}</h1>
      </div>
    )
  })
}

export default SideBar
