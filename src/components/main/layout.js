import React from "react"
import PropTypes, { element } from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import Nav from "../navigation/nav"
import Seo from "./seo"
import Assets from "./assets"

/*
import "../../assets/css/bootstrap.min.css"
import "../../assets/css/mdb.min.css"
*/

import "../../assets/css/main.css"
import Feature from "../blocks/feature"

const Layout = ({ children, subTitle, feature, featureImg}) => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `
  )
  return (
    <>
      <Seo title={subTitle} />
      <div className="layout flex">
        <Header siteTitle={site.siteMetadata.title}>
          <Nav></Nav>
        </Header>
        {feature || featureImg ? <Feature img={featureImg}>{feature}</Feature> : null
        }
        
        <main>{children}</main>
      </div>
      <Assets />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  subTitle: PropTypes.string,
}

export default Layout
