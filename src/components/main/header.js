import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

const Header = ({ siteTitle, children}) => (
  <header
    className="flex"
  >
    <div className="nav-wrapper">
      <nav>
        <Link
          to="/"
          className="title"
        >
          {siteTitle}
        </Link>
        {children}
      </nav>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string.isRequired,
}


export default Header
