import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import NavButtons from "../navigation/nav-buttons"
import Project from "../blocks/projectBlock"
import { jssPreset } from "@material-ui/styles"

const FeaturedProjects = () => {
    const data = useStaticQuery(graphql`
    query {
      allStrapiProject(filter: {featured: {eq: true}}) {
        nodes {
          id
          title
          slug
          technologies {
            id
            color
            name
          }
          associations {
            name
          }
          cover {
            childImageSharp {
              fluid(maxWidth: 150) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
    
  `)

  return (
    <div className="featured-projects">
      <h2>Featured projects</h2>
      <div className="">
        {
          data.allStrapiProject.nodes.map(project => {
            return (
              <div key={project.id} className="featuredProductWrapper inline-block">
                <Project
                  title={project.title}
                  cover={project.cover}
                  associations={project.associations}
                  technologies={project.technologies}
                  slug={project.slug}
                ></Project>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}

export default FeaturedProjects
