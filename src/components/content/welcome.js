import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import NavButtons from "../navigation/nav-buttons"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
 */


const Welcome = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "me.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 550) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <div className="welcome-wrapper">
      <Img fluid={data.placeholderImage.childImageSharp.fluid} />
      <div className="flex margin-top-base">
        <NavButtons />
      </div>
    </div>
  )
}

export default Welcome
