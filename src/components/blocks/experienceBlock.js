import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Tag from "./tag"

const Experience = ({ data }) => {
  return (
    <div className="experience content">
      <div>
        {data.position + " at " + data.association.name}
      </div>
      <div>
        {new Date(data.startDate).toLocaleDateString()} - {new Date(data.endDate).toLocaleDateString()}
      </div>
      <div>
        <Tag color={data.experience_type.color}>{data.experience_type.name}</Tag>
      </div>
      
    </div>
  )
}

Experience.propTypes = {
  data: PropTypes.object.isRequired,
}

export default Experience
