import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

const Tag = ({ dark, color, children, secondary }) => {
  return (
    <div className="tag inline-block" style={{
        backgroundColor: color,
        color: dark ? null : "white"
      }}>
      {children}
    </div>
  )
}

Tag.propTypes = {
  children: PropTypes.string.isRequired,
}

Tag.defaultProps = {
  dark: true,
  secondary: false

}


export default Tag
