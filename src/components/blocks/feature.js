import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { useState } from 'react';
import Img from "gatsby-image"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'

const Feature = ({ img, children }) => {
  const [imageExtended, setImageExtended] = useState(false);

  const extendImage = () => {
    setImageExtended(!imageExtended)
  }
  
  return (
    <div className="full-width feature-wrapper">
      {children ? 
        <div className="feature-content">{children}</div>
      :null}
      {img ?
        <div className={imageExtended ? "feature-image fit-content" : "feature-image"} onClick={extendImage}>
          <Img fluid={img.childImageSharp.fluid} />
          <div className="icon">
            <div className="square">
              <div className="square-content">
                <FontAwesomeIcon icon={faAngleDown}/>
              </div>
            </div>
          </div>
        </div> : null
      }
      
    </div>
    
  )
}

export default Feature
