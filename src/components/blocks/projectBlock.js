import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Tag from "./tag"
import Img from "gatsby-image"

const Project = ({
  title,
  cover,
  technologies,
  associations,
  slug
}) => {
  return (
    <div className="project content">
      <div className="flex">
        {cover ?
        <div className="square margin-right-base">
          <div className="square-content">
            <Img fluid={cover.childImageSharp.fluid}></Img>
          </div>
        </div>
        :null}
        <div>
          <h4>
            {title}
          </h4>
          <p>for {associations.length !== 0 ? associations.map((e,i) => {
            if(i === associations.length-1){
              return e.name
            }else{
              return (e.name + ", ")
            }
          }):"personal use"}</p>
        </div>
      </div>
      <div>
        <div className="spacer margin-top-base">Technologies
          <a href={"/projects/" + slug}>{"Read more >>"}</a>
        </div>
        <div>
          {technologies.map(e => {
            return(
              <Tag key={e.id} color={e.color} dark={false}>
                {e.name}
              </Tag>
            )
          })}
        </div>
      </div>
    </div>
  )
}

Project.propTypes = {
  title: PropTypes.string.isRequired,
  cover: PropTypes.object,
  associations: PropTypes.array,
  technologies: PropTypes.array.isRequired,
}


export default Project