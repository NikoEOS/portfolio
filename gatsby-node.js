const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const queryResults = await graphql(`
    query AllStrapiProject {
      allStrapiProject {
        nodes {
          id
          slug
        }
      }
    }
  `);
  const projectTemplate = path.resolve(`src/templates/project.js`);
  queryResults.data.allStrapiProject.nodes.forEach(node => {
    createPage({
      path: `/projects/${node.slug}`,
      component: projectTemplate,
      context: {
        id: node.id
      }
    });
  });
};
